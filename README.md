# Black Brut Box
Black Brut Box : dérivé de la Malinette et de la BrutBox.
Outils open source basé sur Pure Data essentiellement qui permettent de créer des projets interactifs et musicaux.

## Start
pd -prefsfile settings/bbb.pdsettings -open main.pd

## Mapping ideas
- light-noise-filters
- 4pot-4vol-players
- pencil-scratcher
- pressure-loop-drums
- button-drum
- capa-players triggers
- distance-looper end
- smartphone ?
- seq : short samples

## Settings
- Global settings (./settings/global.conf) : 
	- patch (relative path)
	- rec folder (absolute path)
	- build (0-1)
	- load samples (0-1)
- Project settings : 
	- projects/<PROJECT>/settings/ : samples (abs paths), devices (names id port)
	- projects/<PROJECT>/data/sensors/ : sensors setup

BUILD : delete/create devices abstractions and banks of samples.

## Dependancies
Install : cyclone zexy tof else iemlib iemguts pduino puremapping list-abs


count
- cyclone/switch

time(zexy)

- tof/menu
- else/dir
- else/receiver
- iemlib/splitfilename
- iemguts/initbang
- iemguts/receivecanvas
- cyclone/gate
- pduino/arduino
- puremapping/mean_n

synth-noise
- cyclone/pink~

synth-filters
- cyclone/svg~

seq
- list-abs/list-group 24
- cyclone/uzi
- tof/path
- hcs/stat
- list-abs/list-enumerate

seq16

mem

